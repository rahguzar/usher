;;; usher.el --- Usher in temporary buffers to proper places -*- lexical-binding: t; -*-

;; Copyright (C) 2024 Rahguzar

;; Author: Rahguzar <rahguzar@zohomail.eu>
;; Maintainer: Rahguzar <rahguzar@zohomail.eu>
;; Created: March 31, 2024
;; Version: 0.1
;; License: GPL-3.0-or-later
;; Keywords: convenience tools popups display-buffers
;; Homepage: https://codeberg.org/rahguzar/usher
;; Package-Requires: ((emacs "26.3") (compat "29.1"))

;; This file is not part of GNU Emacs.

;;; Commentary:

;;  This package provides means for managing pop up buffers through
;; `display-buffer-alist'.

;;; Code:
(require 'cl-lib)

(setf (alist-get 'usher-side window-persistent-parameters) t)
(setf (alist-get 'usher-popup window-persistent-parameters) t)
(setf (alist-get 'mode-line-format window-persistent-parameters) t)

(defvar usher-min-dimension '(80 . 20)
  "Cons cell (WIDTH . HEIGHT) determining the minimum size of popup windows.")

(defvar usher-default-parameter-list
  '(:side bottom :size 0.4 :select deselect :kill t :quit t :mode-line none))

(defvar-keymap usher-mode-map)

(defvar usher--buffer-parameters nil)

(define-minor-mode usher--mode
  "Minor mode managed by `usher-display-popup-buffer'."
  :interactive nil
  :keymap usher-mode-map)

(defun usher--window-children (window)
  "Return all children of WINDOW."
  (when-let ((next (window-child window))
             (children (list next)))
    (while (setq next (window-next-sibling next))
      (push next children))
    (nreverse children)))

(defun usher--find-real-window (&optional except)
  "Find a window which is not a popup or side window and not the window EXCEPT."
  (cl-find-if (lambda (win) (not (or (window-parameter win 'usher-side)
                                     (window-parameter win 'window-side)
                                     (eq win except))))
              (window-list)))

(defun usher--delete-window (window)
  "Delete a popup WINDOW."
  (let ((buffers (window-prev-buffers window)))
    (push `(,(window-buffer window)) buffers)
    (setf (window-parameter window 'delete-window) nil)
    (let* ((parent (window-parent window))
           (children (delq window (usher--window-children parent))))
      (when (length= children 1)
        (set-window-parameter (car children) 'usher-side
                              (window-parameter parent 'usher-side))))
    (delete-window window)
    (dolist (buffer buffers)
      (setq buffer (car buffer))
      (when (usher-get :kill buffer)
        (kill-buffer buffer)))))

(defun usher--maybe-delete-real-window (window)
  "Delete WINDOW unless it is the only non-popup and non-side window."
  (if-let ((new-win (usher--find-real-window window)))
      (progn (set-window-parameter
              new-win 'delete-window #'usher--maybe-delete-real-window)
             (set-window-parameter window 'delete-window nil)
             (delete-window window))
    (error "Cannot delete the only window which is not a popup window")))

(defmacro usher--set-window-parameters (window &rest params)
  "Set window PARAMS for WINDOW."
  (declare (indent 1))
  (let (forms)
    (while params
      (push `(set-window-parameter ,window ,(pop params) ,(pop params)) forms))
    `(progn . ,forms)))

(defun usher--get (key parameters)
  "Get the parameter corresponding to KEY from plist PARAMETERS."
  (if (memq key parameters)
      (plist-get parameters key)
    (plist-get usher-default-parameter-list key)))

(defun usher-get (key &optional buffer)
  "Get the value of parameter corresponding to KEY in BUFFER."
  (usher--get key (buffer-local-value
                  'usher--buffer-parameters (or buffer (current-buffer)))))

(defun usher--select-window (window)
  "Select WINDOW if it is a live window."
  (when (and (window-live-p window) (not (active-minibuffer-window)))
    (select-window window)))

(defun usher--split-window-and-display (win buffer alist side)
  "Split window WIN using SIDE and display BUFFER in it with ALIST."
  (let* ((display-win (split-window win nil (pcase side
                                              ('bottom 'below)
                                              ('top 'above)
                                              (_ side)))))
    (if-let (uside (window-parameter win 'usher-side))
        (progn (set-window-parameter win 'usher-side nil)
               (set-window-parameter (window-parent win) 'usher-side uside))
      (set-window-parameter display-win 'usher-side side))
    (window--display-buffer buffer display-win 'window alist)))

(defun usher--display-popup-buffer (buffer alist)
  "Display BUFFER in a popup window according to ALIST."
  (set-window-parameter (usher--find-real-window)
                        'delete-window #'usher--maybe-delete-real-window)
  (let* ((parameters (alist-get 'usher-parameters alist))
         (side (or (alist-get 'side alist) (usher--get :side parameters) 'bottom))
         (vertp (memq side '(top bottom)))
         display-win)
    (setf (alist-get 'side alist) side)
    (cl-callf (lambda (x) (if x x (usher--get :size parameters)))
        (alist-get (if vertp 'window-height 'window-width) alist))
    (if-let ((win (window-with-parameter 'usher-side side nil t)))
        (let* ((children (or (usher--window-children win) `(,win)))
               (new-size (/ (window-size win vertp)
                            (1+ (length children)))))
          (if (> new-size (if vertp
                              (car usher-min-dimension)
                            (cdr usher-min-dimension)) )
              (setq display-win
                    (usher--split-window-and-display
                     win buffer alist (if vertp 'right 'below)))
            (let (dtime)
              (setq display-win (pop children)
                    dtime (window-use-time display-win))
              (dolist (child children)
                (when (< (window-use-time child) dtime)
                  (setq display-win child
                        dtime (window-use-time display-win))))
              (set-window-dedicated-p display-win nil)
              (setq display-win (window--display-buffer
                                 buffer display-win 'reuse alist)))))
      (setq display-win (usher--split-window-and-display
                         (window-main-window) buffer alist side)))
    (prog1 display-win
      (window-bump-use-time display-win)
      (usher--set-window-parameters display-win
        'usher-popup side
        'usher-quit (usher--get :quit parameters)
        'delete-window #'usher--delete-window
        'mode-line-format (usher--get :mode-line parameters)
        'window-side nil)
      (set-window-dedicated-p display-win 'usher))))

(defun usher-display-popup-buffer (buffer alist)
  "Display BUFFER in a popup window according to ALIST."
  (let ((parameters (alist-get 'usher-parameters alist))
        (display-win (if (get-buffer-window buffer)
                         (display-buffer-reuse-window buffer alist)
                       (usher--display-popup-buffer buffer alist))))
    (with-current-buffer buffer
      (setq-local usher--buffer-parameters parameters)
      (usher--mode))
    (when-let ((select (usher--get :select parameters)))
      (run-at-time 0 nil #'usher--select-window
                   (if (eq select 'deselect) (selected-window) display-win)))
    display-win))

(defun usher-quit ()
  "Quit some popup windows.
If called from a popup window, quit that window if it has non-nil `usher-quit'
paramater.  If called from outside a popup or from a pop with nil `usher-quit'
quit all windows with `usher-quit' window parameter equal to t."
  (interactive)
  (if (window-parameter nil 'usher-quit)
      (quit-window)
    (dolist (win (window-list))
      (when (eq t (window-parameter win 'usher-quit))
        (quit-window nil win)))))

(defun usher-quit-all (arg)
  "Quit all quittable popup windows on the selected frame.
Quittable windows are those that have a non-nil `usher-quit' parameter.  In
addtion a popup window is always quittable if it is seelcted.  If prefix ARG is
non-nil quit even those popup windows that are not quittable."
  (interactive "P")
  (when (window-parameter nil 'usher-popup)
    (quit-window))
  (dolist (win (window-list))
    (when (and (window-parameter win 'usher-popup)
               (or arg (window-parameter win 'usher-quit)))
      (quit-window nil win))))

(defun usher-make-rule (&rest kwargs)
  "Make a rule with KWARGS usable as the `ACTION' argument to `display-buffer'.
See `usher-add-rule' for REST ALIST SIDE SIZE SIZE SELECT KILL MODE-LINE QUIT.
This function is useful for producing actions used by specific functions."
  (declare (advertised-calling-convention
            (&rest rest &key alist side size select kill mode-line quit) "0.1"))
  (let ((alist (plist-get kwargs :alist)))
    (setf (alist-get 'usher-parameters alist) kwargs)
    `(usher-display-popup-buffer . ,alist)))

(defun usher-add-rule (condition &rest rest)
  "Add a rule to handle popup buffers matching CONDITION.
ALIST is the action alist as in `display-buffer-alist'.

SIDE specifies side on which the pop will appear.  If SIDE is `top' or `bottom',
the SIZE specifies the height of new window.  If SIDE is `right' or `left',
SIZE specifies the width.  SIDE and SIZE can also be specified using `side' and
`window-height' and `window-width' keys of ALIST.  See Info node
`(elisp) Buffer Display Action Alists' for the details of their format.

If SELECT is non-nil select the window display the buffer.  KILL if non-nil
means to kill the buffer when quitting the popup window.  If MODE-LINE is
`none' don't show the mode-line.  QUIT specifies what should `usher-quit' do
with the pop window created.

REST is a plist containing parameters which can be recovered using `usher-get'."
  (declare (advertised-calling-convention
            (condition &rest rest &key alist side size select kill mode-line quit)
            "0.1"))
  (push `(,condition . ,(apply #'usher-make-rule rest)) display-buffer-alist))

(defun usher-add-rule-to-ignore (condition &optional action alist)
  "Add a rule to not manage temporary buffers matching CONDITION.
ACTION is an action function or a list of action functions, while ALIST is the
action alist both as in `display-buffer-alist'.  The buffer will be displayed
according to ACTION and ALIST."
  (push `(,condition ,action . ,alist) display-buffer-alist))

(defun usher-add-fallback-rule ()
  "Add a rule to to catch all buffer starting with space or *.
The rules displays them according to `usher-default-parameter-list'.  It is
added as the last entry in `display-buffer-alist' to it doesn't any more
specific rules."
  (cl-callf append display-buffer-alist
    `((,(rx bos (or " " "*")) (usher-display-popup-buffer)))))

(provide 'usher)
;;; usher.el ends here
