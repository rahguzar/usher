# usher.el: An usher for temporary buffers in Emacs
[![GPL 3](https://img.shields.io/badge/license-GPLv3-blue.svg)](COPYING)

This package provides a display function `usher-display-popup-buffer` for conveniently dealing with temporary buffers in Emacs. This function can be used directly in `display-buffer-alist` or more conveniently through `usher-add-rule` which will add entries to `display-buffer-alist`.

## Adding rules to `display-buffer-alist`
The first argument to `usher-add-rule` is a condition which can be passed to `buffer-match-p`. It adds an entry to `display-buffer-alist` for buffers that satisfy this condition. The rest of the keyword arguments describe how such a buffer is managed.

- `:side` describe the side of the frame on which the buffer will be displayed. It can be `right`, `left`, `top` or `bottom`.
- `:size` determines the width of the buffer if `:side` is `left` or `right`. If the `:side` is `top` or `bottom` if determines the window height.
- `:select` determines if the window displaying popup buffer is selected. If it is the symbol `deselect` the window is deselected. If it is any other non-nil value, the window is selected. `nil` means not to interfere with the selection.
- `:kill` non-nil means to kill the buffer when deleting the popup window.
- `:mode-line` determines the `mode-line` parameter of the window. If it is `none` no mode-line is displayed. If it is nil the `mode-line-format` of the displayed buffer is used. Otherwise it overrides the `mode-line-format`.
- `:quit` if non-nil makes it easier to quit the popup window displaying this buffer. Calling `usher-quit` from any window will quit such a window.

The default values for the keyword arguments are determined by `usher-default-parameter-list`.

## Window management
`usher-display-popup-buffer` displays buffers in special windows that are managed by it. There can one such window on each side of the main window on a frame.

If such a window exists and another buffer needs to be displayed on the same side, the window can be split. For a popup window at top or bottom, the original window will be split vertical for right or left side, the split will be horizontally. Such a split is done only if each of these windows can occupy enough space. The required size is determined by `usher-min-dimension` variable.

If splitting is not possible, the buffer is instead displayed in the least recently used window on that side.

## Usage
Here is some of configuration:
```emacs-lisp
(setq display-buffer-alist nil)
(usher-add-fallback-rule)
(usher-add-rule (rx bos "*" (or "info" "eww" "scratch" "Proced") "*")
                :side 'right :size '(body-columns . 80) :select t :kill nil :mode-line nil :quit nil)
(usher-add-rule (rx bos (? " ") "*" (or "Minibuf" "Messages")) :select nil :kill nil)
(usher-add-rule (rx bos "*backtrace*") :select t)
(usher-add-rule-to-ignore
 (rx bos (and "*" (or "eshell" "Ibuffer") "*")) #'display-buffer-same-window)
```

## Acknowledgments
This package is heavily inspired by the popup system of Doom Emacs. However it differs in some ways:
- It does not take over `display-buffer-alist`, rather it just provides some means of managing it.
- They dynamic splitting of popup windows is not present in Doom Emacs popup system. Rather it is inspired by some of the layouts in some tiling window managers such as `xmonad`.
